<?php
    include "../lib/header.php";
    include "../database.php";
    $client_id = $_GET['client_id'];
    $sql = "SELECT * FROM tbl_client WHERE client_id = :client_id;";
            $stat = $conn->prepare($sql);
            $stat->execute([':client_id' => $client_id]);
            $data = $stat->fetch(PDO::FETCH_OBJ);
     // SELECT cmpny.name,clnt.client_name,clnt.contact_num,clnt.client_id FROM tbl_company cmpny JOIN tbl_client clnt ON cmpny.company_id = clnt.company_id;

        //company_selection
        $sql = "SELECT * FROM tbl_company";
        $stat = $conn->prepare($sql);
        $stat->execute();
        $data1 = $stat->fetchall(PDO::FETCH_OBJ);

        //update_client
        if(isset($_POST['update'])){

            $contact_name   = $_POST['contact_name'];
            $contact_num    = $_POST['contact_num'];
            $company_id   = $_POST['company_id'];
            // $user_id        = "";
            // $userprofile    = ""; 

                 $sql = 'UPDATE tbl_client SET client_name=:name, contact_num=:contact_num, company_id=:company_id WHERE client_id=:id';
                      // ':user_id'     => $user_id,
                    $stat = $conn->prepare($sql);
                    $stat->execute([':name'=>$contact_name,':contact_num'=>$contact_num,':company_id'=>$company_id, ':id' => $client_id]);
                    // ':user_id'     => $user_id,
                    // ':image'       => $userprofile

                    header("Location: admin_client.php");
        }
 



            

    //     if(isset($_POST['submit'])){
    //     $errMSG = "";

    //     $company_name = $_POST['Company_Name'];
    //     $company_address = $_POST['Company_Address'];

    //     if(empty($company_name)){
    //         $Cname_errMSG = "Please enter Company name.";
    //         // echo $errMSG;
    //     }
    //     else if(empty($company_address))
    //     {
    //         $Cadd_errMSG = "Please enter your Company address.";
    //         // echo $Cadd_errMSG;
    //     }

    //     if(empty($Cname_errMSG) && empty($Cadd_errMSG)){
         
    //     $sql = 'INSERT INTO tbl_company(name,address) VALUES(:comp_name,:comp_address)';

    //     $stat = $conn->prepare($sql);

    //     $stat->execute([
    //         ':comp_name'    => $company_name,
    //         ':comp_address' => $company_address
    //                     ]);
        
    //     header("Refresh:0");
    //     }
    // }
?>
<div class="row">
    <div class="col-md-12">
        <span> 
    <form action="" method="post">
        <center>
            <div class="col-md-12">
                 <div class="col-md-2"></div>
                 <div class="comp col-md-12">
                    <h3><?= $data->client_name; ?>'s Data</h3>
                   <ul>
                        <li>
                            <label for="Contact Name">Contact Name: </label>
                            <input value="<?= $data->client_name; ?>" type="text" name="contact_name" id="Contact Name" placeholder="Contact Name"> 

                                <?php if(isset($Cname_errMSG)){ echo $Cname_errMSG; }?> 
                            </li>
                        <li><label for="Contact Number">Contact Number:</label>
                            <input value="<?= $data->contact_num; ?>" type="text" name="contact_num" id="Contact Number" placeholder="Contact Number"> 
                                <?php if(isset($Cadd_errMSG)){echo $Cadd_errMSG;}?>
                            </li>
                        <li>
                            <label>Company Name:</label>
                                <select name="company_id">
                                   <option>choose company</option>
                                    <?php foreach($data1 as $value):?>
                                    <option  <?=($value->company_id ==  $data->company_id)?'selected':''; ?>  value="<?= $value->company_id; ?>" ><?= $value->company_name; ?>    
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </li>
                        <li><input type="submit" name="update" id="submit" value="Update">
                            </li>
                    </ul>
                 </div>
                 <div class="col-md-2"></div>
            </div>
             
        </center>       
    </form>


   <!--          <div class="container-fluid">
                <div class="row">
                <div class="col-md-12">
                <form action="client_company.php" method="get" id="<?= $data->company_id; ?>">
                 
                </form>
                 </div>
            </div>
        </div> -->



                        </span>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar, #content').toggleClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
        </script>
 <?php include "../lib/footer.php";?>