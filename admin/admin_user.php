<?php
    include "../lib/header.php";
    include "../database.php";
    $sql = 'SELECT * FROM tbl_user';

            $stat = $conn->prepare($sql);
            $stat->execute();
            $list = $stat->fetchall(PDO::FETCH_OBJ);

        

        if(isset($_GET['id'])){

                $id = $_GET['id'];
                $sql = "DELETE FROM tbl_user WHERE user_id=:id";
                $stat = $conn->prepare($sql);
                $stat->execute([
                                ':id'=>$id]
                                );    
                header("Location: admin_user.php");
            }

        if(isset($_POST['submit'])){
        $errMSG = "";

        $user_name     = $_POST['user_name'];
        $user_email    = $_POST['user_email'];
        $user_password = $_POST['user_password'];

        if(empty($user_name)){
            $Cname_errMSG = "Please enter User name.";
            // echo $errMSG;
        }
        else if(empty($user_email))
        {
            $Cadd_errMSG = "Please enter your User email.";
            // echo $Cadd_errMSG;
        }
         else if(empty($user_password))
        {
            $Cpass_errMSG = "Please enter your password";
            // echo $Cadd_errMSG;
        }

        if(empty($Cname_errMSG) && empty($Cadd_errMSG)){
         
        $sql = 'INSERT INTO tbl_user(name,email,password) VALUES(:user_name,:user_email,:user_password)';

        $stat = $conn->prepare($sql);

        $stat->execute([
            ':user_name'    => $user_name,
            ':user_email' => $user_email,
            'user_password' => $user_password
                        ]);
        
        header("Refresh:0");
        // header("Location: admin_user.php");
        }
    }

     if(isset($_POST['search'])){
            $searrch_name = $_POST['user_name'];
            // $search = "p";
            $sql = "SELECT * FROM tbl_user WHERE name LIKE :search";
            $stat = $conn->prepare($sql);
            $stat->execute([':search' => '%'.$searrch_name.'%']);
            $list = $stat->fetchall(PDO::FETCH_OBJ);
            
             // print_r($list);

         }
?>


<div class="row">
    <div class="col-md-12">
        <span>    

    <form action="" method="post">
                        <center>
                            <div class="col-md-12">
                                 <div class="col-md-2"></div>
                                 <div class="comp col-md-12">
                                    <h3>User</h3>
                           <ul>
                                <li>
                                    <label for="User Name">User Name: </label>
                                    <input type="text" name="user_name" placeholder="User Name"> 
                                    <?php if(isset($Cname_errMSG)){ echo $Cname_errMSG; }?> </li>
                                <li><label for="User email">User email: </label>
                                    <input type="text" name="user_email" id="User email" placeholder="User email"> 
                            <?php if(isset($Cadd_errMSG)){echo $Cadd_errMSG;}?> 
                            </li>
                            <li><label for="User password">User password: </label>
                                    <input type="text" name="user_password" id="user_password" placeholder="******"> 
                            <?php if(isset($Cpass_errMSG)){echo $Cpass_errMSG;}?> 
                            </li>
                                <li><input type="submit" name="submit" id="submit" value="Add"></li>
                            </ul>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        </center>       
    </form>
 
 <form action="admin_user.php" method="post">
    <div class="row">
        <div class="user col-md-12">
            <div class="col-md-6"></div>
            <div class="col-md-6"> 
                <input type="text" name="user_name" id="user_name" placeholder="user_Name"> 
                <input type="submit" class="btn" name="search" value ="Search"> 
            </div>
        </div>
    </div>
 </form>
 

            <div class="container-fluid">
                <div class="row">
                <div class="col-md-12">
                <form action="admin_user.php" method="get" id="<?= $data->user_id; ?>">
                    <table  style="width: 100%; border: 1px solid black;" >
                            <tr>
                                <th>User Name</th>
                                <th>User email</th>
                                <th>Password</th>
                                <th>Action</th>
                            </tr>
                        <?php foreach($list as $data): ?>   
                            <tr >
                                <td>
                                    <label for="username" ><?= $data->name; ?></a></label> 
                                </td>
                                <td>
                                    <label for="username" ><?= $data->email; ?></label>
                                </td>
                                <td>
                                    <label for="username" ><?= $data->password; ?></label>
                                </td>

                                <td>
                        <a class="btn btn-primary" href="user_edit.php?id=<?= $data->user_id; ?>">Edit</a> 

                         <input type="hidden" name="id" value="<?= $data->user_id; ?>" /><input type="submit" id="<?= $data->user_id; ?>" class="btn" name="Delete" value ="Delete"> 
                                </td>
                        </tr>
                        <?php endforeach; ?>
                        <br>  
                    </table>
                </form>
                 </div>
            </div>
        </div>



                        </span>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar, #content').toggleClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                });
            });
        </script>

        
 <?php include "../lib/footer.php";?>